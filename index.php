<?php

require './libs/DbConnect.php';
$db = new DbConnect();

if (isset($_REQUEST)) {
    $action = $_REQUEST['action'];
    if ($action == "login") {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $result = $db->loginCheck($email, $password);
        if (!empty($result)) {
            echo json_encode($result);
        } else {
            echo "error";
        }
    }
    if ($action == "register") {
        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $password = trim($_POST['password']);
        $response = $db->addUsers($name, $email, $password);
        if (!$response) {
            echo "error";
        } else {
            echo "success";
        }
    }

    if ($action == "change_password") {
        $user_id = trim($_POST['id']);
        $password = trim($_POST['password']);
        $new_password = trim($_POST['new_password']);
        $response = $db->passCheck($user_id, $password);
        if (!empty($response)) {
            $db->changePassword($user_id, $new_password);
            echo "success";
        } else {
            echo "error";
        }
    }
    if ($action == "syncAtoS") {
        print_r($_POST);
        $user_id = trim($_POST['id']);
        $account = trim($_POST['account']);
        $payment = trim($_POST['payment']);

        $accountData = json_decode($account, true);
        $paymentData = json_decode($payment, true);
      

        $db->deleteAccounts($user_id);
        $db->deletePayment($user_id);
        foreach ($accountData as $data) {
            $account_id = $data['id'];
            $name = $data['name'];
            $email = $data['email'];
            $phone = $data['mobile'];
            $description = $data['description'];
            $db->addAccount($account_id,$user_id, $name, $email, $phone, $description);
        }
        foreach ($paymentData as $data) {
           $payment_id = $data['id'];
            $amount = $data['amount'];
            $account = $data['account'];
            $date = $data['date'];
            $type = $data['type'];
            $note = $data['note'];
            $db->addPayment($payment_id,$user_id, $account, $amount, $type, $note, $date);
        }
        echo $accountData_length;
    }
    if ($action == "syncStoA") {
        $user_id = trim($_POST['id']);
        $payment_data = $db->getPayment($user_id);
        $account_data = $db->getAccount($user_id);
        echo json_encode(array(
            "payment" => $payment_data,
            "account" => $account_data
                )
        );
    }
}

?>
