<?php

class DbConnect {

    protected $db;

    public function __construct() {

        $host = "localhost";
        $user = "root";
        $password = "";
        $database = "mybook";

        $this->db = new mysqli($host, $user, $password, $database);
        if ($this->db->connect_errno > 0) {
            die('Unable to connect to database [' . $this->db->connect_error . ']');
        }
    }

    public function getData($query) {

        $data = array();
        if ($result = $this->db->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        } else {
            die('There was an error running the query [' . $this->db->error . ']');
            return FALSE;
        }
    }

    public function setData($query) {
        if ($result = $this->db->query($query)) {
            return $result;
        } else {
            die('There was an error running the query [' . $this->db->error . ']');
            return FALSE;
        }
    }

    public function getUsers($id = "") {
        if ($id == "") {
            $query = "Select * from user";
        } else {
            $query = "Select * from `user` where id = '$id'";
        }
        return $this->getData($query);
    }

    public function addUsers($name, $email, $password) {
        $query = "INSERT INTO user set name = '$name', email = '$email', password = '$password'";
        return $this->setData($query);
    }

    public function loginCheck($email, $password) {
        $query = "SELECT id,name FROM user WHERE email = '$email' AND password = '$password'";
      return $this->getData($query);
    }
     public function passCheck($id, $password) {
        $query = "SELECT id,name FROM user WHERE id = '$id' AND password = '$password'";
      return $this->getData($query);
    }
    public function changePassword($id,$password){
          $query = "UPDATE user set password = '$password' WHERE id = '$id'";
        return $this->setData($query);
    }
    public function getAccount($id) {
        $query = "SELECT * FROM accounts where user_id = '$id'";
        return $this->getData($query);
    }
       public function addAccount($id,$user_id,$name, $email, $phone,$description) {
        $query = "INSERT INTO accounts set account_id = '$id', user_id = '$user_id', name = '$name', email = '$email', phone = '$phone', description = '$description'";
        return $this->setData($query);
    }
    public function deleteAccounts($id){
          $query = "DELETE FROM accounts where user_id = '$id'";
        return $this->setData($query);
    }
      public function getPayment($id) {
        $query = "SELECT * FROM payment where user_id = '$id'";
        return $this->getData($query);
    }
     public function addPayment($id,$user_id, $account_id, $amount,$type,$note,$date) {
        $query = "INSERT INTO payment set payment_id = '$id',user_id = '$user_id', account_id = '$account_id', amount = '$amount', type = '$type', note = '$note',date = '$date'";
        return $this->setData($query);
    }
    public function deletePayment($id){
          $query = "DELETE FROM payment where user_id = '$id'";
        return $this->setData($query);
    }
    

}
